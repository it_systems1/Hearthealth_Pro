# Cardiovascular Risk Prediction

## Overview:

This Cardiovascular Risk Prediction Application is designed to predict the likelihood of individuals developing heart diseases. The web app takes various health and clinical indicators as input and estimates cardiovascular risk. It is particularly useful during counselling sessions with individuals with no known cardiovascular conditions, allowing for early risk identification and prevention strategies.

## Health Indicators used:

* Age
* Sex
* Chest pain type
* Resting blood pressure
* Fasting blood sugar > 120 mg/dl
* Resting electrocardiographic results
* Exercise-induced angina
* Serum cholesterol
* Maximum heart rate achieved
* ST depression induced by exercise relative to rest
* Slope of the peak exercise ST segment
* Number of major vessels colored by fluoroscopy
* Thalassemia


## Tools Used:
* Python
* Django
* Pandas, Numpy, Seaborn, Scikit-learn
* HTML, CSS, JavaScript

## Directory:

### Assignment 4: 
* Designed the front end of the application. Created using the HTML CSS, JS.

### Assignment 5: 
* Created the Random Forest model to predict the Heart Disease health risk using the scikit learn.
* Used the model along with Django to predict the health disease risk by taking input from the users.

![Alt text](https://gitlab.com/it_systems1/Hearthealth_Pro/-/raw/main/image/Picture1.png)

![Alt text](https://gitlab.com/it_systems1/Hearthealth_Pro/-/raw/main/image/Picture2.png)
