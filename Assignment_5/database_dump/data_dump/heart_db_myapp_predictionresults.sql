-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: localhost    Database: heart_db
-- ------------------------------------------------------
-- Server version	8.0.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `myapp_predictionresults`
--

DROP TABLE IF EXISTS `myapp_predictionresults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `myapp_predictionresults` (
  `prediction_id` int NOT NULL AUTO_INCREMENT,
  `prediction_result` varchar(10) NOT NULL,
  `record_id_id` int NOT NULL,
  `risk_probability` double NOT NULL,
  PRIMARY KEY (`prediction_id`),
  KEY `myapp_predictionresu_record_id_id_2ad49db4_fk_myapp_med` (`record_id_id`),
  CONSTRAINT `myapp_predictionresu_record_id_id_2ad49db4_fk_myapp_med` FOREIGN KEY (`record_id_id`) REFERENCES `myapp_medicalrecords` (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `myapp_predictionresults`
--

LOCK TABLES `myapp_predictionresults` WRITE;
/*!40000 ALTER TABLE `myapp_predictionresults` DISABLE KEYS */;
INSERT INTO `myapp_predictionresults` VALUES (3,'risk',13,0.9489911820214658),(4,'risk',14,0.570092872811585),(5,'risk',15,0.9324448104107694),(6,'risk',16,0.9571272700875965),(7,'risk',17,0.8698986714178608),(8,'risk',18,0.8684817757407364),(9,'risk',19,0.751014714374161),(10,'risk',20,0.951721798946482),(11,'no risk',21,0.01455648024738528),(12,'no risk',22,0.01455648024738528),(13,'no risk',23,0.01455648024738528),(14,'no risk',24,0.01455648024738528),(15,'risk',25,0.9440669180798295),(16,'risk',26,0.7940176441356377),(17,'risk',27,0.7940176441356377),(18,'risk',28,0.7940176441356377),(19,'risk',29,0.9214355133586749),(20,'risk',30,0.9872817959427673),(21,'risk',31,0.9693449239624002);
/*!40000 ALTER TABLE `myapp_predictionresults` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-12  8:00:19
