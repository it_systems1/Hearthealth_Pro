-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: localhost    Database: heart_db
-- ------------------------------------------------------
-- Server version	8.0.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `myapp_patients`
--

DROP TABLE IF EXISTS `myapp_patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `myapp_patients` (
  `patient_id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `date_of_birth` date NOT NULL,
  `sex` varchar(10) NOT NULL,
  `height` double NOT NULL,
  `weight` double NOT NULL,
  `blood_type` varchar(5) NOT NULL,
  `city` varchar(100) NOT NULL,
  `street` varchar(100) NOT NULL,
  `telephone_number` varchar(15) NOT NULL,
  `insurance_number` varchar(50) NOT NULL,
  PRIMARY KEY (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `myapp_patients`
--

LOCK TABLES `myapp_patients` WRITE;
/*!40000 ALTER TABLE `myapp_patients` DISABLE KEYS */;
INSERT INTO `myapp_patients` VALUES (1,'Lukas','Meyer','1988-05-20','Male',175,70,'A+','Potsdam','Heinrich-Mann-Allee 20','+49 331 1234567','DE1234567890123'),(2,'Sophie','Schulze','1995-09-15','Female',165,58,'O-','Cottbus','Marktplatz 15','+49 355 9876543','DE9876543210987'),(3,'Julian','Hoffmann','1983-02-12','Male',180,80,'B+','Brandenburg an der Havel','Rathausplatz 10','+49 3381 234567','DE3456789012345'),(4,'Lea','Wagner','1979-11-28','Female',170,65,'A-','Frankfurt (Oder)','Karl-Marx-Straße 25','+49 335 8765432','DE6789012345678'),(5,'Hannah','Koch','1985-08-30','Female',168,63,'O+','Potsdam','Luisenplatz 50','+49 331 3456789','DE2345678901234'),(6,'Tom','Schneider','1975-07-07','Male',178,78,'A+','Cottbus','Karl-Liebknecht-Straße 5','+49 355 6543210','DE5678901234567'),(7,'Mara','Schwarz','1980-12-12','Female',172,68,'B-','Brandenburg an der Havel','Friedrich-Ebert-Straße 20','+49 3381 765432','DE7890123456789'),(8,'Maximilian','Berger','1987-03-25','Male',185,85,'AB-','Frankfurt (Oder)','Lindenstraße 8','+49 335 876543','DE9876543210987'),(9,'Lina','Krüger','1970-02-01','Female',160,55,'A-','Potsdam','Brandenburger Straße 12','+49 331 987654','DE8765432109876'),(10,'Anna','Weber','1986-10-10','Female',168,62,'B+','Cottbus','Lindenplatz 8','+49 355 5432109','DE6543210987654'),(11,'David','Lange','1984-12-05','Male',183,80,'AB+','Potsdam','Schopenhauerstraße 15','+49 331 4321098','DE5432109876543'),(12,'Tim','Graf','1973-04-03','Male',180,78,'O-','Frankfurt (Oder)','Berliner Straße 30','+49 335 6543210','DE3210987654321'),(13,'Laura','Simon','1997-01-05','Female',170,65,'B+','Oranienburg','Bismarckstraße 5','+49 3301 876543','DE2109876543210'),(14,'Paula','Voigt','1982-09-20','Female',166,60,'AB-','Cottbus','Heinrich-Heine-Straße 25','+49 355 1098765','DE1098765432109'),(15,'Kevin','Kühn','1989-11-14','Male',178,75,'A-','Potsdam','Kurfürstenstraße 100','+49 331 2109876','DE0987654321098'),(16,'Jan','Lorenz','1995-07-10','Male',180,80,'B-','Frankfurt (Oder)','Karl-Marx-Allee 20','+49 335 8765432','DE8765432109876'),(17,'Eva','Müller','1985-03-12','Female',167,62,'A+','Berlin','Alexanderplatz 5','+49 30 1234567','DE1234567890123'),(18,'Jonas','Schmidt','1990-08-25','Male',180,78,'B-','Berlin','Friedrichstraße 10','+49 30 9876543','DE9876543210987'),(19,'Lara','Wagner','1979-11-05','Female',172,65,'AB+','Potsdam','Karl-Liebknecht-Straße 20','+49 3381 234567','DE3456789012345'),(20,'Niklas','Becker','1988-06-18','Male',175,70,'O-','Cottbus','Potsdamer Platz 15','+49 30 8765432','DE6789012345678'),(21,'Sophia','Weber','1995-09-30','Female',160,55,'A-','Potsdam','Kurfürstendamm 50','+49 30 3456789','DE9012345678901'),(22,'Luca','Zimmermann','1983-04-22','Male',185,80,'B+','Potsdam','Am Markt 3','+49 3382 123456','DE2345678901234'),(23,'Mia','Richter','1975-07-14','Female',168,63,'A-','Cottbus','Unter den Linden 25','+49 30 6543210','DE5678901234567'),(24,'Felix','Lehmann','1980-12-01','Male',178,75,'AB-','Potsdam','Friedrichstraße 20','+49 30 2345678','DE7890123456789'),(25,'Emilia','Krüger','1987-05-28','Female',166,60,'O+','Potsdam','Breite Straße 8','+49 3383 765432','DE9876543210987'),(26,'Max','Schulz','1970-02-11','Male',182,78,'B-','Cottbus','Schlossstraße 30','+49 30 8765432','DE8765432109876'),(27,'Hanna','Hofmann','1993-08-08','Female',170,68,'A+','Potsdam','Kollwitzstraße 15','+49 30 5432109','DE7654321098765'),(28,'Finn','Werner','1978-11-26','Male',177,73,'AB+','Cottbus','Lange Brücke 12','+49 3382 234567','DE6543210987654'),(29,'Lena','Berger','1991-03-17','Female',165,58,'A-','Potsdam','Gendarmenmarkt 8','+49 30 4321098','DE5432109876543'),(30,'Tim','Koch','1986-10-05','Male',180,75,'O-','Potsdam','Oranienburger Straße 40','+49 30 2109876','DE4321098765432'),(31,'Mia','Neumann','1984-12-19','Female',163,61,'B+','Cottbus','Am Bahnhof 5','+49 3381 876543','DE3210987654321'),(32,'Paul','Schäfer','1973-06-30','Male',185,80,'AB-','Potsdam','Friedrichstraße 25','+49 30 6543210','DE2109876543210'),(33,'Leonie','Herrmann','1997-01-14','Female',169,64,'O+','Cottbus','Kurfürstendamm 100','+49 30 7890123','DE1098765432109'),(34,'Luca','Zimmermann','1982-04-22','Male',178,70,'B+','Potsdam','Am Markt 3','+49 3382 123456','DE2345678901234'),(35,'Laura','Vogt','1990-07-15','Female',165,60,'A-','Cottbus','Unter den Linden 100','+49 30 8765432','DE3456789012345'),(36,'Ben','Schwarz','1989-11-02','Male',181,75,'O-','Potsdam','Potsdamer Platz 50','+49 30 5432109','DE4567890123456');
/*!40000 ALTER TABLE `myapp_patients` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-12  8:00:19
