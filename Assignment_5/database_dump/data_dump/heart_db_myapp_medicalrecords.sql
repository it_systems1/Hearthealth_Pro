-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: localhost    Database: heart_db
-- ------------------------------------------------------
-- Server version	8.0.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `myapp_medicalrecords`
--

DROP TABLE IF EXISTS `myapp_medicalrecords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `myapp_medicalrecords` (
  `date` date NOT NULL,
  `record_id` int NOT NULL AUTO_INCREMENT,
  `age` int NOT NULL,
  `sex` int NOT NULL,
  `chest_pain` int NOT NULL,
  `blood_pressure` int NOT NULL,
  `fasting_bs` int NOT NULL,
  `resting_ecg` int NOT NULL,
  `exercise_angina` int NOT NULL,
  `cholesterol` int NOT NULL,
  `heart_rate` int NOT NULL,
  `st_depression` double NOT NULL,
  `slope` int NOT NULL,
  `vessels` int NOT NULL,
  `thal` int NOT NULL,
  `remarks` longtext,
  `patient_id_id` int NOT NULL,
  PRIMARY KEY (`record_id`),
  KEY `myapp_medicalrecords_patient_id_id_253e2809_fk_myapp_pat` (`patient_id_id`),
  CONSTRAINT `myapp_medicalrecords_patient_id_id_253e2809_fk_myapp_pat` FOREIGN KEY (`patient_id_id`) REFERENCES `myapp_patients` (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `myapp_medicalrecords`
--

LOCK TABLES `myapp_medicalrecords` WRITE;
/*!40000 ALTER TABLE `myapp_medicalrecords` DISABLE KEYS */;
INSERT INTO `myapp_medicalrecords` VALUES ('2024-03-09',3,20,0,0,91,0,0,1,3,4,2,0,0,3,'nothing',1),('2024-03-01',4,50,1,2,110,1,2,1,58,58,7,1,0,3,'All is good',7),('2024-03-15',5,32,0,3,90,1,2,1,5,22,10,0,2,6,'good report.',20),('2024-03-16',6,30,1,1,90,0,1,0,4,11,10,1,2,6,'good report.',6),('2024-03-16',7,30,0,2,90,1,2,1,3,55,8,2,3,7,'good report.',6),('2024-03-01',8,25,0,0,96,1,1,0,11,12,5,0,1,6,'*',8),('2024-03-05',9,26,1,1,105,1,2,1,70,48,6,1,2,7,'l',10),('2024-03-05',10,26,1,1,105,1,2,1,70,48,6,1,2,7,'l',10),('2024-03-05',11,26,1,1,105,1,2,1,70,48,6,1,2,7,'l',10),('2024-03-05',12,30,0,0,100,1,1,0,50,50,9,2,2,7,'il',4),('2024-03-05',13,30,0,0,100,1,1,0,50,50,9,2,2,7,'il',4),('2024-03-05',14,53,0,1,100,0,0,0,50,50,9,0,1,3,'il',22),('2024-03-05',15,64,1,0,100,0,0,0,104,73,9,2,1,3,'il',22),('2024-03-05',16,64,1,0,100,0,0,0,104,73,9,2,2,3,'il',22),('2024-03-05',17,80,1,0,135,0,0,0,158,101,10,0,3,7,'il',22),('2024-03-05',18,80,1,0,135,0,0,0,158,101,10,0,3,6,'il',22),('2024-03-05',19,80,1,0,135,0,0,0,158,101,10,0,1,6,'il',22),('2024-03-06',20,45,1,2,180,1,2,1,106,52,8,2,2,6,'l',6),('2024-03-06',21,20,0,0,110,0,0,0,60,50,1,1,0,3,'l',6),('2024-03-06',22,20,0,0,110,0,0,0,60,50,1,1,0,3,'l',6),('2024-03-06',23,20,0,0,110,0,0,0,60,50,1,1,0,3,'l',6),('2024-03-06',24,20,0,0,110,0,0,0,60,50,1,1,0,3,'l',6),('2024-03-07',25,60,0,0,110,1,2,1,150,94,5,2,2,7,'new',12),('2024-03-10',26,35,1,0,135,1,0,1,145,120,6,1,1,3,'',36),('2024-03-10',27,35,1,0,135,1,0,1,145,120,6,1,1,3,'',36),('2024-03-10',28,35,1,0,135,1,0,1,145,120,6,1,1,3,'',36),('2024-03-01',29,35,1,3,135,1,1,1,145,120,6,1,1,6,'no',13),('2024-03-11',30,38,1,3,120,0,2,1,48,60,10,1,2,6,'nothing',12),('2024-03-11',31,38,1,0,120,1,1,1,48,60,10,2,2,6,'nothing',10);
/*!40000 ALTER TABLE `myapp_medicalrecords` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-12  8:00:19
