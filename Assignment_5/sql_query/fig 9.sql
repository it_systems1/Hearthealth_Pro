SELECT c.first_name, c.last_name, a.record_id, a.age,
a.sex, a.blood_pressure, a.cholesterol, a.heart_rate ,
b.prediction_result, b.risk_probability
FROM heart_db.myapp_medicalrecords as a
INNER JOIN heart_db.myapp_predictionresults as b
ON a.record_id = b.record_id_id
INNER JOIN heart_db.myapp_patients as c
ON a.patient_id_id = c.patient_id;